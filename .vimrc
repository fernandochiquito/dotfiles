set nocompatible						"Latest Vim Settings/options

so ~/.vim/plugins.vim					"Source file for plugins in vim
so ~/.vim/coc.vim						"Coc config file

syntax enable
set termguicolors
colorscheme cobalt2
let g:rigel_airline = 1
let g:airline_theme = 'rigel'


"----General Config----"
set backspace=indent,eol,start			"Make backspace behave like every other editor.
let mapleader = ','						"Change Default leader key from '\' to ','.
set number 								"Activate line numbers
set relativenumber 						"Relative nunbers on the line. Easy to make some ajustments.
set guitablabel=%t 						"Tabs with file names instead of full path.
set linebreak							"This will get Vim to wrap existing text as desired
set wrap								"tells Vim to only wrap at a character in the breakat option
set autoindent
set shiftwidth=2  						"indenting is 2 spaces
set tabstop=4							"indent small
" set cursorcolumn						"Show Cursor column position easy for see were is intentation.


"----Visuals----"
set t_CO=256							"Use 256 colors in Vim
set encoding=utf8						"encoding to UTF-8 to show glyphs


"----Search----"
set hlsearch							"Show Search Caracters
set incsearch							"Show Search as I type


"-----Split Management-----"
nmap <C-J> <C-W><C-J>					"Go down on split
nmap <C-K> <C-W><C-K>					"Go up on split
nmap <C-H> <C-W><C-H>					"Go Left on split
nmap <C-L> <C-W><C-L>					"Go Right on Split

set splitbelow							"Always below split on horizontal
set splitright							"Always right split on vertical

"----Mappings----"
nmap <Leader><space> :nohlsearch<cr>	"Turn off highlight search.
nmap <Leader>ev :tabedit $MYVIMRC<cr>	"Easy vimrc
nmap <Leader>sp :vsp<cr>				"Easy Split Vertical
nmap <C-6> :bp

"----Move lines----"
nnoremap <S-Up> :m-2<CR>
nnoremap <S-Down> :m+<CR>
inoremap <S-Up> <Esc>:m-2<CR>
inoremap <S-Down> <Esc>:m+<CR>
" let g:user_emmet_leader_key=','


"----NERDtree----"
autocmd VimEnter * NERDTree				"Start NERDTree and leave the cursor in it.
nnoremap <leader>n :NERDTreeFocus<CR>
noremap <C-n> :NERDTree<CR>
let NERDTreeHijackNetrw = 0				"Give back '-' to vinager


"----Vim Airline----"
let g:airline#extensions#tabline#enabled = 1	"Pritty tabs
let g:airline_powerline_fonts = 1				"Bring arrows to airline


"----Auto-Commands----"
augroup autosourcing
	autocmd!
	autocmd BufWritepost .vimrc source %		"Auto source the Vimrc on save.
augroup END
