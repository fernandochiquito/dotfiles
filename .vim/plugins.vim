" Plugins will be downloaded under the specified directory.
call plug#begin(has('nvim') ? stdpath('data') . '/plugged' : '~/.vim/plugged')

" Declare the list of plugins.
Plug 'tpope/vim-vinegar'
Plug 'preservim/nerdtree'
Plug 'ryanoasis/vim-devicons'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'tpope/vim-surround'
Plug 'mattn/emmet-vim'
Plug 'sheerun/vim-polyglot'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'ap/vim-css-color'
Plug 'thaerkh/vim-indentguides'
Plug 'GlennLeo/cobalt2'
Plug 'Rigellute/rigel'
Plug 'turbio/bracey.vim', {'do': 'npm install --prefix server'}
Plug 'jiangmiao/auto-pairs'


" List ends here. Plugins become visible to Vim after this call.
call plug#end()
